import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {ticketData} from './tickets'
import { Observable } from 'rxjs/Observable';

@Injectable()
export class GetJsonDataService {

  private dataUrl: string = 'assets/data/data.json';

  constructor(private http: HttpClient) { }

  getTicketS():Observable<ticketData[]>{
    return this.http.get<ticketData[]>(this.dataUrl);
  }

}
