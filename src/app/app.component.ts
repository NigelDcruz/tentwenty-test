import { Component, OnInit } from '@angular/core';
import { GetJsonDataService } from './get-json-data.service'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  p: number = 1;

  // Json data array
  public tickets = [];

  // initial values
  public originState = "PNQ";
  public destinationState = "DEL";

  constructor(private dataService: GetJsonDataService) { }

  ngOnInit() {
    //fetching Json from service injector
    this.dataService.getTicketS().subscribe(
      (data) => this.tickets = data
    );
  }

  // for getting the user input
  inputOrigin: string = "PUNE";
  inputDestination: string = "GOA";
  search() {
    // converting user input to uppercase
    var Origin = this.inputOrigin.toUpperCase();
    var Destination = this.inputDestination.toUpperCase();

    // validation check

    if (Origin != "PUNE" && Origin != "GOA" && Origin != "DELHI" && Origin != "MUMBAI") {
      alert("Please Enter A Valid State");
      this.originState = "PNQ"
    }

    if (Destination != "PUNE" && Destination != "GOA" && Destination != "DELHI" && Destination != "MUMBAI") {
      alert("Please Enter A Valid State");
      this.destinationState = "DEL"
    }

    this.inputOrigin = Origin;
    this.inputDestination = Destination;

    // origin state
    if (this.inputOrigin == "DELHI") {
      this.originState = "DEL";
      console.log(this.tickets);
    }
    else if (this.inputOrigin == "GOA") {
      this.originState = "GOA";
    }
    else if (this.inputOrigin == "MUMBAI") {
      this.originState = "MBI"
    }
    else {
      this.originState = "PNQ"
    }

    // destination state
    if (this.inputDestination == "DELHI") {
      this.destinationState = "DEL";
      console.log(this.tickets);
    }
    else if (this.inputDestination == "GOA") {
      this.destinationState = "GOA";
    }
    else if (this.inputDestination == "MUMBAI") {
      this.destinationState = "MBI"
    }
    else {
      this.destinationState = "PNQ"
    }

  }

}