export interface ticketData {
    airline: string,
    airlineCode: string,
    flightNumber: number,
    origin: string,
    availableSeats: number,
    destination: string,
    price: number,
    departureDate: string,
    departureTime: string,
    arrivalDate: string,
    arrivalTime:  string
}